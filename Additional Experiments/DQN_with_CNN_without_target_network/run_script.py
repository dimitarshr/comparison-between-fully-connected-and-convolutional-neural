import subprocess

N_EXPERIMENTS = 5
TRAIN_SEEDS = [0, 1, 2, 3, 4]
TEST_SEEDS = [5, 6, 7, 8, 9]

if __name__ == "__main__":
	for exp_no in range(N_EXPERIMENTS):

		print(exp_no)
		params = ["python", "./DQN_with_CNN_without_target_network_more_frames.py"] + \
				 			  ["--training_seed=" + str(TRAIN_SEEDS[exp_no])] + \
							  ["--testing_seed=" +  str(TEST_SEEDS[exp_no])]
		subprocess.call(params)
