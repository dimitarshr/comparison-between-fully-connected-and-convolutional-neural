import gym
from gym import wrappers
import numpy as np
import random
from collections import deque
from keras.models import Sequential, load_model
from keras.layers import Dense
from keras.optimizers import Adam
import matplotlib.pyplot as plt
import datetime
import csv

# ENVIRONMENT_NAME = 'Acrobot-v1'
 ENVIRONMENT_NAME = 'LunarLander-v2'
# ENVIRONMENT_NAME = 'CartPole-v0'
MEMORY_MAX_SIZE = 2**16
MEMORY_MIN_SIZE = 2**6
BATCH_SIZE = 2**5
GAMMA = 0.99 # discount rate
EPSILON = 1.0  # exploration rate
EPSILON_MIN = 0.0
EPSILON_DECAY = 0.998
LEARNING_RATE = 0.0001
TEST_EPISODES = 100
AVERAGE_SCORE = 200
# TRAINED_MODEL = "/home/dimitar/Desktop/ml-experiments/LunarLander/models/2019-02-02 12:59:15.239834.h5"
TRAINED_MODEL = ""

# Deep Q-learning Agent
class DQNAgent:
    def __init__(self, state_space, action_space, environment):
        self.state_space = state_space
        self.action_space = action_space
        self.gamma = GAMMA
        self.epsilon = EPSILON
        self.epsilon_min = EPSILON_MIN
        self.epsilon_decay = EPSILON_DECAY
        self.learning_rate = LEARNING_RATE
        self.model = self._build_model()
        self.environment = environment
        self.memory = deque([], maxlen=MEMORY_MAX_SIZE)
        self.running_rewards = deque([], maxlen=100)
        self.xAxisValues = []
        self.yAxisValues = []
        self.isPlotInitialised = False
        self.memoryInfo = True
        self.dataToCSV = []

    def _build_model(self):
        # Neural Net for Deep-Q learning Model
        model = Sequential()
        model.add(Dense(128, input_dim=self.state_space, activation='relu'))
        model.add(Dense(64, activation='relu'))
        model.add(Dense(self.action_space, activation='linear'))
        model.compile(loss='mean_squared_error',
                      optimizer=Adam(lr=self.learning_rate))
        return model

    # Keeps a recond of everything seen so far.
    def remember(self, state, action, reward, next_state, done):
        # Informative update on the memory size
        if len(self.memory) == MEMORY_MAX_SIZE and self.memoryInfo:
            self.memoryInfo = False
            print ('\tMemory has reached '+str(MEMORY_MAX_SIZE)+' tuples of expericences.')
        self.memory.append((state, action, reward, next_state, done))

    # Chooses an action to be perfomed.
    def act(self, state, training):
        if training and np.random.rand() <= self.epsilon:
            return random.randrange(self.action_space)
        # If the model is being evaluated,
        # the agent should only take learnt actions.
        else:
            # convert the state into a 1 x state_size matrix
            reshaped = state.reshape((1, self.state_space))
            # converts the 'reshaped' into a list and passes it to predict
            # predict returns a list of Numpy array
            # and argmax returns the index of the max value
            return np.argmax(self.model.predict([reshaped]))

    def replay(self):
        # Cheking that there are enough memory tuples
        # before sampling them
        if len(self.memory) < BATCH_SIZE:
            return
        if len(self.memory) < MEMORY_MIN_SIZE:
            return
        minibatch = random.sample(self.memory, BATCH_SIZE)
        # construct arrays for states(S), actions(A), rewards(R),
        #                      next_state(T) and done(D)
        S = np.array([e[0] for e in minibatch])     # takes the state values
        A = np.array([e[1] for e in minibatch])     # takes the action values
        R = np.array([e[2] for e in minibatch])     # takes the reward vales
        T = np.array([e[3] for e in minibatch])     # takes the next_state values
        D = np.array([e[4] for e in minibatch])     # takes the done values

        # predict the Q values for the states
        q = self.model.predict(S)
        # predict the Q values for the next_states
        q_t = self.model.predict(T)
        # create a matrix of 0s with dimensions 'len(minibatch) x action_space'
        y = np.zeros((len(minibatch), self.action_space))
        for i in xrange(len(minibatch)):
            # takes the reward, action and Q value based on their order
            r, a, t = R[i], A[i], q[i]
            # if Done
            if D[i]:
                t[a] = r
            # otherwise, calculate the discounted Q value
            else:
                t[a] = r + self.gamma * np.max(q_t[i])
            # update he matrix of 0s to contain the new Q values
            # based on the action
            y[i] = t

        # fit takes the states and the updated Q values
        # in order to calculatet the loss function and
        # to adjust the weights of the NN
        self.model.fit(S, y, batch_size=BATCH_SIZE, epochs=1, verbose=0)

    def train(self):
        self.run(training=True)

    def run(self, training=False):
        self.dataToCSV = []
        if not training:
            self.isPlotInitialised = False
            self.running_rewards.clear()
            if TRAINED_MODEL == "":
                #record the results every 10 episodes
                self.environment = wrappers.Monitor(self.environment, './videos/{}'.format(str(datetime.datetime.now())), \
                    force=True, video_callable=lambda episode_id: episode_id%10==0)

        if not self.isPlotInitialised:
            self.initialise_plot(training)
            self.isPlotInitialised = True

        episodeCounter = 0
        running = True
        while running:
            episodeScore = 0
            frames = 0
            episodeCounter += 1
            state = self.environment.reset()
            done = False
            while not done:
                # each iterations is a single frame
                frames += 1
                # Render if we are not training
                if not training:
                    env.render()
                action = self.act(state, training)
                next_state, reward, done, _ = self.environment.step(action)
                episodeScore += reward
                if training:
                    self.remember(state, action, reward, next_state, done)
                    # train the NN
                    self.replay()
                # make next_state the new current state for the next frame.
                state = next_state

            # Store the score of the current episode
            self.running_rewards.append(episodeScore)

            # Calculate the average score for the last 100 episodes
            mean_reward = self.mean_reward()

            if training:
                # Reduce the epsilon at the end of the episode
                self.reduce_epsilone()
                # update the values for the line graph
                self.update_plot(episodeCounter, mean_reward)
                print '#{}, Reward: {:.2f}, Epsilon: {:.2f}; Frames: {}; Running: {:.2f}' \
                .format(episodeCounter, episodeScore, self.epsilon, frames, mean_reward)
                # add headers
                if (len(self.dataToCSV) == 0):
                    self.dataToCSV.append(["Episode_No", "Episode_score", "Epsilon", "No_of_frames",
                        "Mean_reward_for_the_past_100_episodes"])
                # add data to be written to CSV file
                self.dataToCSV.append([episodeCounter, episodeScore, self.epsilon, frames, mean_reward])
            else:
                # update the values for the line graph
                self.update_plot(episodeCounter, episodeScore)
                print '#{}, Reward: {:.2f}, Frames: {}; Running: {:.2f}' \
                .format(episodeCounter, episodeScore, frames, mean_reward)
                # add headers
                if (len(self.dataToCSV) == 0):
                    self.dataToCSV.append(["Episode_No", "Episode_score", "No_of_frames",
                        "Mean_reward_for_the_past_100_episodes"])
                # data to be written to CSV file
                self.dataToCSV.append([episodeCounter, episodeScore, frames, mean_reward])

            # Train the NN until the average score from the last 100 episodes is >= 200
            # once finished stop the while loop and draw the line graph
            if training and mean_reward >= AVERAGE_SCORE:
                self.draw_plot(training)
                self.writeToCSV(training, self.dataToCSV)
                running = False
            elif not training and episodeCounter >= TEST_EPISODES:
                # if the used model was not pretrained
                if TRAINED_MODEL == "":
                    self.draw_plot(training)
                    self.writeToCSV(training, self.dataToCSV)
                    self.save_model()
                running = False

    def initialise_plot(self, training):
        self.xAxisValues = []
        self.yAxisValues = []

        if training:
            plt.subplot(2, 1, 1)
            plt.title(ENVIRONMENT_NAME + " training")
            plt.xlabel('episode')
            plt.ylabel('average score for the past \n100 episodes')
        else:
            plt.subplot(2, 1, 2)
            plt.title(ENVIRONMENT_NAME + " testing")
            plt.xlabel('episode')
            plt.ylabel('score')
        plt.tight_layout()

    def update_plot(self, xValue, yValue):
        self.xAxisValues.append(xValue)
        self.yAxisValues.append(yValue)

    def draw_plot(self, training):
        lineGraphColour = ""
        if training:
            lineGraphColour = "blue"
        else:
            lineGraphColour = "red"
            plt.axhline(y=self.mean_reward(), color='black', linestyle='-.')
        plt.plot(self.xAxisValues, self.yAxisValues, c=lineGraphColour)
        # plt.draw() # to be vialised in read time
        # plt.pause(0.005)
        plt.axhline(y=AVERAGE_SCORE, color='darkgreen', linestyle='--')

    def reduce_epsilone(self):
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def mean_reward(self):
        return np.mean(self.running_rewards)

    def save_model(self):
        self.model.save("./models/{}.h5".format(str(datetime.datetime.now())))

    def writeToCSV(self, training, data):
        fileName = fileName = './CSVdata/{}Testing.csv'.format(str(datetime.datetime.now()))

        if training:
            fileName = './CSVdata/{}Training.csv'.format(str(datetime.datetime.now()))

        with open(fileName, 'w') as writeFile:
            writer = csv.writer(writeFile)
            writer.writerows(data)

        writeFile.close()


if __name__ == "__main__":
    # initialise gym environment and the agent
    env = gym.make(ENVIRONMENT_NAME)
    agent = DQNAgent(env.observation_space.shape[0], env.action_space.n, env)

    if TRAINED_MODEL == "":
        # train the agent until it overall scores converge
        agent.train()
    else:
        agent.model = load_model(TRAINED_MODEL)

    print("\t\t Starting testing!")

    # test the trained model
    agent.run()

    if TRAINED_MODEL == "":
        # save the line graph as a PNG image
        plt.savefig('myfig.png', dpi=1000)
